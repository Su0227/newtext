import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './router/index'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'//element样式
import axios from 'axios'
require('./mock')

Vue.prototype.$ajax = axios
Vue.config.productionTip = false

Vue.use(ElementUI)
Vue.use(VueRouter)
new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
