import Home from '../pages/Home'
import About from '../pages/About'
import Echarts from '../pages/Echarts'

import VueRouter from 'vue-router'


export default new VueRouter({
    routes:[
        {
            path:'/Home',
            component:Home
        },
        {
            path:'/About',
            component:About
        },
        {
            path:'/Echarts',
            component:Echarts
        },
    ]
})
